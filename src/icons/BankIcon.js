import React from 'react';

class BankIcon extends React.PureComponent {
    render() {
        return <svg aria-hidden="true" viewBox="0 0 24 24" style={{ width: '28px', fill: '#002846' }}>
            <path d="M7 21h-4v-11h4v11zm7-11h-4v11h4v-11zm7 0h-4v11h4v-11zm2 12h-22v2h22v-2zm-23-13h24l-12-9-12 9z"/>
        </svg>;
    }
}

export default BankIcon;
