import React from 'react';
import axios from 'axios';

import { formatCurrency, formatDate } from '../../components/formatter';
import { statusLabels } from '../../mappers';
import Address from '../../components/address';
import BoxIcon from '../../icons/BoxIcon';
import BreadCrumb from '../../components/breadcrumb';
import ClockIcon from '../../icons/ClockIcon';
import EditIcon from '../../icons/EditIcon';
import NameInitials from '../../components/name-initials';
import OrderItems from '../../components/order-items';
import UserIcon from '../../icons/UserIcon';

import './order.scss';

class Order extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            orderId: props.match.params.orderId,
            order: null,
            isLoading: true,
        };
    }

    componentDidMount() {
        axios.get(`/api/orders/${this.state.orderId}`)
            .then(response => this.setState({
                order: response.data,
                nextLink: response.data.nextLink,
                isLoading: false,
            }))
            .catch(this.onGetOrderError.bind(this))
    }

    onGetOrderError() {
        alert('Não foi possível recuperar a lista de pedidos');

        // FIXME: improve error handling

        this.setState({
            isLoading: false
        });
    }

    render() {
        const {order} = this.state;

        return <div className="container">
            <BreadCrumb orderId={this.state.orderId} />

            <h1>Detalhes do pedido</h1>

            {this.state.isLoading && 'Recuperando os dados pedido...'}
            {!this.state.isLoading && <div className="order">
                <div className="row">
                    <div className="order__amount">
                        <span className="order__amount-currency">R$</span>
                        <span className="order__amount-value">{formatCurrency(order.amount.total, true)}</span>
                    </div>
                    <div className="order__date">
                        <div className="order__date--name">Data</div>
                        <div className="order__date--value">{formatDate(order.createdAt)}</div>
                    </div>
                </div>

                <div className="row order-row">
                    <div className="order-id">
                        <BoxIcon className="order-id__icon" />

                        <div className="order-id__content">
                            <div className="order-id__label">Pedido</div>
                            <div className="order-id__value">{order.id}</div>
                        </div>
                    </div>

                    <div className="order-status">
                        <ClockIcon className="order-status__icon" /> {statusLabels[order.status]}
                    </div>
                </div>

                <div className="row buyer">
                    <div className="buyer__header">
                        <div className="buyer__header-title">
                            <EditIcon className="buyer__header-icon buyer__header-icon--small"/>
                            Dados do Comprador
                        </div>

                        <UserIcon className="buyer__header-icon" />
                    </div>

                    <div className="buyer__name">
                        <NameInitials name={order.customer.fullname} showLabel={true} />
                    </div>

                    <div className="buyer__address">
                        <Address address={order.shippingAddress || order.customer.addresses[0]} />
                    </div>
                </div>

                <div className="row">
                    <h2 className="order-details__title">Detalhes do Pedido</h2>

                    <div className="order-details">
                        <OrderItems items={order.items} />
                    </div>
                </div>
            </div>}
        </div>;
    }
}

export default Order;
