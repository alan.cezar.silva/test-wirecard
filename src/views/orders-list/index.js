import React from 'react';

import { get } from '../../components/http';
import BreadCrumb from '../../components/breadcrumb';
import OrdersTable from '../../components/orders-table/orders-table';
import ScrollingChart from '../../components/scrolling-chart';
import './orders-list.scss';
import LoadingChart from '../../components/loading-chart';
import OrderTableSkeleton from '../../components/orders-table/order-table-skeleton';

class OrdersList extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            nextLink: '',
            isLoading: false,
        };
    }

    componentDidMount() {
        get('api/orders')
            .then(response => this.setState({
                orders: response.data.orders,
                nextLink: `/api/${response.data.nextLink}`,
                isLoading: false,
            }))
            .catch(this.onGetOrdersError.bind(this))
    }

    getMore() {
        get(this.state.nextLink)
            .then(response => this.setState({
                orders: [...this.state.orders, ...response.data.orders],
                nextLink: `/api/${response.data.nextLink}`,
                isLoading: false,
            }))
            .catch(this.onGetOrdersError.bind(this))
    }

    onGetOrdersError() {
        alert('Não foi possível recuperar a lista de pedidos');

        // FIXME: improve error handling

        this.setState({
            isLoading: false
        });
    }

    convertOrdersToPoints = () =>this.state.orders.map(order => order.amount.total);

    render() {
        return <div className="container">
            <BreadCrumb />

            <h1>Pedidos</h1>

            {
                !this.state.orders ?
                    <>
                        <LoadingChart></LoadingChart>
                        <OrderTableSkeleton></OrderTableSkeleton>
                    </> :
                    <>
                        <ScrollingChart points={this.convertOrdersToPoints()}></ScrollingChart>
                        <div className="table-wrapper">
                            <OrdersTable orders={this.state.orders} />
                        </div>
                    </>
            }

            {this.state.nextLink && <div className="button-group">
                <button onClick={this.getMore.bind(this)}>Mais Resultados</button>
                <button onClick={() => window.scrollTo({ top: 0, behavior: 'smooth' })}>⇑</button>
            </div>}
        </div>
    }
}

export default OrdersList;
