import React from 'react';
import { Redirect } from 'react-router-dom';
import MoipIcon from '../../icons/MoipIcon';

import './login.scss';

class Login extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            redirect: false,
        };
    }

    handleUserName(event) {
        this.setState({
            username: event.target.value,
        });
    }

    handlePassword(event) {
        this.setState({
            password: event.target.value,
        });
    }

    handleLoginClick() {
        this.setState({
            redirect: true,
        });
    }

    render() {
        const { password, username } = this.state;

        if (this.state.redirect) {
            return <Redirect to="/"/>
        }

        return <form className="wrapper">
            <MoipIcon className="icon"></MoipIcon>

            <input placeholder="login ou e-mail" type="text" onChange={this.handleUserName.bind(this)} />
            <input placeholder="senha" type="password" onChange={this.handlePassword.bind(this)} />

            <button disabled={!password || !username} onClick={this.handleLoginClick.bind(this)}>Acessar minha conta</button>

            <a>Esqueci minha senha</a>

            <a>Criar uma nova conta</a>
        </form>
    }
}

export default Login;
