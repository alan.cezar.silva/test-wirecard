import React from 'react';
import { shallow } from 'enzyme';
import BreadCrumb from '.';

it('should not crash', () => void shallow(<BreadCrumb />));

it('should render only the orders at initial settings', () => {
    const initialLayout = <nav className="breadcrumb">
        Início / <span className="breadcrumb__page">Pedidos</span>
    </nav>;

    expect(shallow(<BreadCrumb />)).toContainReact(initialLayout);
});

it('should render only the orders at initial settings', () => {
    const initialLayout = <nav className="breadcrumb">
        Início / <span className="breadcrumb__page">Pedidos</span>
    </nav>;

    expect(shallow(<BreadCrumb />)).toContainReact(initialLayout);
});

it('should render only the orders at initial settings', () => {
    const layoutWithOrderID = <nav className="breadcrumb">
        Início / Pedidos / <span className="breadcrumb__page">ABCDEF</span>
    </nav>;

    expect(shallow(<BreadCrumb orderId="ABCDEF" />)).toContainReact(layoutWithOrderID);
});

