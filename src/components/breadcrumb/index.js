import React from 'react';

import './breadcrumb.scss';

class BreadCrumb extends React.PureComponent {
    render() {
        return this.props.orderId ? 
            <nav className="breadcrumb">
                Início / Pedidos / <span className="breadcrumb__page">{this.props.orderId}</span>
            </nav> :
            <nav className="breadcrumb">
                Início / <span className="breadcrumb__page">Pedidos</span>
            </nav>;
    }
}

export default BreadCrumb;
