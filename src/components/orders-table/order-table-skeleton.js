import React from 'react';
import Skeleton from '../skeleton';

class OrderTableSkeleton extends React.PureComponent {
    render() {
        return <table className="orders-table">
            <thead>
                <tr>
                    <td><abbr title="ID do pedido">Pedidos</abbr></td>
                    <td><abbr title="Valor">R$</abbr></td>
                    <td><abbr title="Meio de Pagamento">Meio</abbr></td>
                    <td>Status</td>
                    <td>Data</td>
                    <td>Cliente</td>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td>
                        <Skeleton style={{ width: '160px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '100px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '60px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '110px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '175px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '150px' }}></Skeleton>
                    </td>
                </tr>
                <tr>
                    <td>
                        <Skeleton style={{ width: '160px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '50px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '60px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '110px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '175px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '135px' }}></Skeleton>
                    </td>
                </tr>
                <tr>
                    <td>
                        <Skeleton style={{ width: '160px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '80px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '60px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '110px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '175px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '175px' }}></Skeleton>
                    </td>
                </tr>
                <tr>
                    <td>
                        <Skeleton style={{ width: '160px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '50px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '60px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '110px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '175px' }}></Skeleton>
                    </td>
                    <td>
                        <Skeleton style={{ width: '100px' }}></Skeleton>
                    </td>
                </tr>
            </tbody>
        </table>
    }
}

export default OrderTableSkeleton;
