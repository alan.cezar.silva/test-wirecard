import React from 'react';
import { Link } from 'react-router-dom';

import CardIcon from '../../icons/CardIcon';
import BarCodeIcon from '../../icons/BarCodeIcon';
import BankIcon from '../../icons/BankIcon';
import { formatCurrency, formatDate } from '../formatter';
import { paymentMethods, statusLabels } from '../../mappers';

import './orders-table.scss';
import NameInitials from '../name-initials';

class OrdersTable extends React.PureComponent {
    render() {
        return <table className="orders-table">
            <thead>
                <tr>
                    <td><abbr title="ID do pedido">Pedidos</abbr></td>
                    <td><abbr title="Valor">R$</abbr></td>
                    <td><abbr title="Meio de Pagamento">Meio</abbr></td>
                    <td>Status</td>
                    <td>Data</td>
                    <td>Cliente</td>
                </tr>
            </thead>

            <tbody>
                {this.props.orders.map((order, index) => <tr key={index} id={`order-${index * 10}`}>
                        <td>
                            <Link to={`orders/${order.id}`}>{order.id}</Link>
                        </td>
                        <td className="orders-table__currency">{formatCurrency(order.amount.total)}</td>
                        <td title={paymentMethods[order.payments[0].fundingInstrument.method]}>
                            {order.payments[0].fundingInstrument.method === 'CREDIT_CARD' && <CardIcon />}
                            {order.payments[0].fundingInstrument.method === 'DEBIT_CARD' && <CardIcon />}
                            {order.payments[0].fundingInstrument.method === 'BOLETO' && <BarCodeIcon />}
                            {order.payments[0].fundingInstrument.method === 'ONLINE_BANK_FINANCING' && <BankIcon />}
                            {order.payments[0].fundingInstrument.method === 'ONLINE_BANK_DEBIT' && <BankIcon />}
                            {order.payments[0].fundingInstrument.method === 'WALLET' && <BankIcon />}
                        </td>
                        <td className="orders-table__status">
                            <div className={`orders-table__status orders-table__status--${order.status}`}>
                                {statusLabels[order.status]}
                            </div>
                        </td>
                        <td className="orders-table__date">{formatDate(order.createdAt)}</td>
                        <td className="orders-table__client">
                            <NameInitials name={order.customer.fullname} />
                        </td>
                    </tr>
                )}
            </tbody>
        </table>
    }
}

export default OrdersTable;
