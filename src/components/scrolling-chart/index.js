import React from 'react';

import './scrolling-chart.scss';

const line = (pointA, pointB) => {
    const lengthX = pointB[0] - pointA[0];
    const lengthY = pointB[1] - pointA[1];

    return {
        length: Math.sqrt(Math.pow(lengthX, 2) + Math.pow(lengthY, 2)),
        angle: Math.atan2(lengthY, lengthX),
    };
}

const controlPoint = (current, previous, next, reverse) => {
    const previousPoint = previous || current
    const nextPoint = next || current

    const o = line(previousPoint, nextPoint)

    const angle = o.angle + (reverse ? Math.PI : 0)
    const length = o.length * .1

    const x = current[0] + Math.cos(angle) * length;
    const y = current[1] + Math.sin(angle) * length;

    return [x, y];
}

const bezierCommand = (point, i, a) => {
    const startControlPoint = controlPoint(a[i - 1], a[i - 2], point)
    const endControlPoint = controlPoint(point, a[i - 1], a[i + 1], true)

    return `C ${startControlPoint[0]},${startControlPoint[1]} ${endControlPoint[0]},${endControlPoint[1]} ${point[0]},${point[1]}`
}

const getSvgPath = (points, command) => points.reduce((acc, point, index, a) => index === 0
      ? `M ${point[0]},${point[1]}`
      : `${acc} ${command(point, index, a)}`
    , '');

class ScrollingChart extends React.PureComponent {
    constructor(props) {
        super(props);

        this.svgPath = React.createRef();
        this.onScroll = this.onScroll.bind(this);
    }

    formatPoints(points) {
        const greatestPoint = Math.max(...points);

        return points.map((point, index) => [
            index * 10,
            30 - Math.round((point / greatestPoint) * 30) || 2
        ]);
    }

    onScroll() {
        const svgPath = this.svgPath.current;

        if (!svgPath) return;

        const length = svgPath.getTotalLength();

        svgPath.style.strokeDasharray = length;
        svgPath.style.strokeDashoffset = length;
        svgPath.style.transition = 'all ease .3s';

        const { clientHeight, scrollHeight, scrollTop } = document.documentElement
        const scrollpercent = (document.body.scrollTop + scrollTop) / (scrollHeight - clientHeight);
        const draw = length * scrollpercent;

        svgPath.style.strokeDashoffset = length - draw;
    }

    scrollToPoint(point) {
        const row = document.getElementById(`order-${point[0]}`);

        row.classList.remove('pulse')
        row.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
        row.classList.add('pulse')
    }

    componentDidMount() {
        const svgPath = this.svgPath.current;

        if (!svgPath) return;

        const length = svgPath.getTotalLength();

        svgPath.style.strokeDasharray = length;
        svgPath.style.strokeDashoffset = length;

        window.addEventListener('scroll', this.onScroll, false);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.onScroll, false);
    }

    render() {
        const viewBox = `0 0 ${this.props.points.length * 10} 32`;
        const points = this.formatPoints(this.props.points);

        return <div className="scrolling-chart">
            <svg viewBox={viewBox} preserveAspectRatio="xMidYMid meet" className="chart">
                <path
                    d={getSvgPath(points, bezierCommand)}
                    fill="none"
                    transform="translate(4, 0)"
                    stroke="#989898"
                    strokeWidth="1px" />

                <path
                    ref={this.svgPath}
                    d={getSvgPath(points, bezierCommand)}
                    fill="none"
                    transform="translate(4, 0)"
                    stroke="#2a5cbc"
                    strokeWidth="2px" />

                {points.map((point, index) => <circle
                    key={`circle${index}`}
                    onMouseEnter={() => this.scrollToPoint(point)}
                    cx={point[0]}
                    cy={point[1]}
                    r="3" />
                )}
            </svg>
        </div>
    }
}

export default ScrollingChart;
