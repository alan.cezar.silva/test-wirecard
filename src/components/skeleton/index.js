import React from 'react';

import './skeleton.scss';

class Skeleton extends React.PureComponent {
  render() {
    return <div className="skeleton" style={this.props.style}>
      <div className="skeleton--animated" style={this.props.style}></div>
    </div>;
  }
}

export default Skeleton;
