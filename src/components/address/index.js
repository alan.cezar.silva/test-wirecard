import React from 'react';

import HomeIcon from '../../icons/HomeIcon';
import './address.scss';

class Address extends React.PureComponent {
    render() {
        const {
            city,
            complement,
            district,
            state,
            street,
            streetNumber,
            zipCode,
        } = this.props.address;

        return <address className="address">
            <HomeIcon className="address__icon" />

            <div className="address__wrapper">
                <h2 className="address__title">Endereço de Entrega</h2>

                <div>{street}, {streetNumber}{complement && `, ${complement}`}</div>
                <div>{district}</div>
                <div>{city} - {state}</div>
                <div>CEP - {zipCode}</div>
            </div>
        </address>
    }
}

export default Address;
