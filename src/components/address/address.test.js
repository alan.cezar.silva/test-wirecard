import React from 'react';
import { shallow } from 'enzyme';
import Address from '.';

const mockAddress = {
    city: 'Luziânia',
    complement: '',
    country: 'BRA',
    district: 'Setor Mandú 2',
    state: 'Goiás',
    street: 'Rua 05 Q 16 Lote 33',
    streetNumber: '33',
    type: 'BILLING',
    zipCode: '72814540',
};

const mockAddressWithComplement = {
    city: 'Luziânia',
    complement: 'bloco 3 ap 52',
    country: 'BRA',
    district: 'Setor Mandú 2',
    state: 'Goiás',
    street: 'Rua 05 Q 16 Lote 33',
    streetNumber: '33',
    type: 'BILLING',
    zipCode: '72814540',
};

it('should not crash', () => void shallow(<Address address={mockAddress} />));

it('should render address information', () => {
    const address = shallow(<Address address={mockAddress} />);
    const street = <div>{`${mockAddress.street}, ${mockAddress.streetNumber}`}</div>;
    const zipCode = <div>{`CEP - ${mockAddress.zipCode}`}</div>;
    const cityAndState = <div>{`${mockAddress.city} - ${mockAddress.state}`}</div>;

    expect(address).toContainReact(street);
    expect(address).toContainReact(zipCode);
    expect(address).toContainReact(cityAndState);
});

it('should render address information with complement', () => {
    const address = shallow(<Address address={mockAddressWithComplement} />);
    const street = <div>{`${mockAddress.street}, ${mockAddress.streetNumber}, ${mockAddressWithComplement.complement}`}</div>;
    const zipCode = <div>{`CEP - ${mockAddress.zipCode}`}</div>;
    const cityAndState = <div>{`${mockAddress.city} - ${mockAddress.state}`}</div>;

    expect(address).toContainReact(street);
    expect(address).toContainReact(zipCode);
    expect(address).toContainReact(cityAndState);
});
