import React from 'react';
import { shallow } from 'enzyme';
import NameInitials from '.';

it('should not crash', () => void shallow(<NameInitials name="Fulano de Tal" />));

it('should show the name', () => {
    const component = shallow(<NameInitials name="Fulano de Tal" />);

    expect(component.text()).toContain('Fulano de Tal');
});

it('should show the first letter in upper case', () => {
    const component = shallow(<NameInitials name="Fulano de Tal" />);
    const result = <div className="name__initial">F</div>;

    expect(component).toContainReact(result);
});

it('should show label when asked for', () => {
    const component = shallow(<NameInitials name="Fulano de Tal" showLabel={true} />);
    const result = <div className="name__label">Nome</div>;

    expect(component).toContainReact(result);
});
