import React from 'react';

import './name-initials.scss';

class NameInitials extends React.PureComponent {
    render() {
        return <div className="name">
            <div className="name__initial">{this.props.name.substring(0, 1).toUpperCase()}</div>
            
            <div className="name__value">
                {this.props.showLabel && <div className="name__label">Nome</div>}
                {this.props.name}
            </div>
        </div>
    }
}

export default NameInitials;
