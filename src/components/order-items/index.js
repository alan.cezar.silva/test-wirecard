import React from 'react';

import { formatCurrency } from '../formatter';
import './order-items.scss';

class OrderItems extends React.PureComponent {
    render() {
        const totalValue = this.props.items.reduce((accumulator, item) => accumulator + item.price, 0);

        return <React.Fragment>
            <ul className="items">
                {this.props.items.map((item, index) => <li className="item" key={`orderItem-${index}`}>
                    {item.product} {formatCurrency(item.price)}
                </li>)}
            </ul>

            <div className="items__total-title">Valor total dos itens</div>
            <div className="items__total-value">
                R$ <span className="items__total-price-tag">{formatCurrency(totalValue, true)}</span>
            </div>
        </React.Fragment>;
    }
}

export default OrderItems;

