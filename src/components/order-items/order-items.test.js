import React from 'react';
import { shallow } from 'enzyme';
import OrderItems from '.';

it('should not crash', () => void shallow(<OrderItems items={[]} />));
