import React from 'react';

import './loading-chart.scss';

class LoadingChart extends React.PureComponent {
    render() {
        return <div className="loading-chart">
            <svg viewBox="0 0 200 32">
                <path
                    className="loading-path"
                    fill="none"
                    strokeWidth="1px"
                    stroke="#2a5cbc"
                    d="M 10,5 C 11,6 18,15 20,15 C 22,15 28,4.5 30,5 C 32,5.5 38,20 40,20
                    C 42,20 48,4.5 50,5 C 52,5.5 58,24 60,25 C 62,26 68,15 70,15 C 72,15 78,26 80,25
                    C 82,24 88,6 90,5 C 92,4 98,14.5 100,15 C 102,15.5 108,10 110,10 C 112,10 118,14 120,15
                    C 122,16 128,20.5 130,20 C 132,19.5 138,11.5 140,10 C 142,8.5 148,4.5 150,5 C 152,5.5 159,14 160,15"></path>
            </svg>
        </div>;
    }
}

export default LoadingChart;
