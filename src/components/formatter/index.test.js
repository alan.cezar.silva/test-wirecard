import { formatCurrency, formatDate } from '.';

it('should format currency values', () => {
    const expected = new Intl.NumberFormat('pt-BR', {
        style: 'currency',
        currency: 'BRL',
    }).format('190.00');

    expect(formatCurrency(19000)).toEqual(expected);
    expect(formatCurrency(1000)).toContain('10.00');
    expect(formatCurrency(10000000)).toContain('100,000.00');
});

it('should format currency values without currency symbol', () => {
    const expected = new Intl.NumberFormat('pt-BR', {
        currency: 'BRL',
        minimumFractionDigits: 2,
    }).format('190.00');

    expect(formatCurrency(19000, true)).toEqual(expected);
    expect(formatCurrency(1000, true)).toContain('10.00');
    expect(formatCurrency(10000000, true)).toContain('100,000.00');
});

it('should return an zero price on falsy values', () => {
    expect(formatCurrency(null)).toEqual('R$ 0.00');
    expect(formatCurrency(undefined)).toEqual('R$ 0.00');
    expect(formatCurrency('')).toEqual('R$ 0.00');
});

it('should format date values', () => {
    expect(formatDate('2019-06-20T22:09:47-0300')).toEqual('2019/06/20 • 22:09');
});

it('should return an empty string on falsy values', () => {
    expect(formatDate(null)).toEqual('');
    expect(formatDate(undefined)).toEqual('');
    expect(formatDate('')).toEqual('');
});
