export const formatDate = date => date ?
    `${date.substr(0, 10).replace(/-/g, '/')} • ${date.substr(11, 2)}:${date.substr(14, 2)}` :
    '';

export const formatCurrency = (value, hideCurrency = false) => {
    let result = String(value);

    if (!value) return 'R$ 0.00';
    
    result = result.substring(0, result.length - 2) + '.' + result.substring(2);

    return hideCurrency ?
        new Intl.NumberFormat('pt-BR', {
            currency: 'BRL',
            minimumFractionDigits: 2,
        }).format(result) :
        new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL',
        }).format(result)
};
