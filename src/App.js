import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.scss';

import Login from './views/login';
import Order from './views/order';
import OrdersList from './views/orders-list';

function App() {
  return (
    <Router className="App">
      <Route exact path="/" component={OrdersList} />
      <Route path="/login" component={Login} />
      <Route path="/orders" exact component={OrdersList} />
      <Route path="/orders/:orderId" exact component={Order} />
    </Router>
  );
}

export default App;
