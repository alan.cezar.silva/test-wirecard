export const statusLabels = {
    WAITING: 'Aguardando pagamento',
    NOT_PAID: 'Pagamento atrasado',
    PAID: 'Pedido pago',
}

export const paymentMethods = {
    CREDIT_CARD: 'Cartão de Crédito',
    DEBIT_CARD: 'Cartão de Débito',
    BOLETO: 'Boleto',
    ONLINE_BANK_FINANCING: 'Financiamento Bancário Online',
    ONLINE_BANK_DEBIT: 'Débito em Conta Corrente',
    WALLET: 'Carteira Digital',
}
