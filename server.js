const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');
const axios = require('axios');
const app = express();

axios.defaults.baseURL = 'https://sandbox.moip.com.br/v2';
axios.defaults.headers.common['Authorization'] = 'Basic MDEwMTAxMDEwMTAxMDEwMTAxMDEwMTAxMDEwMTAxMDE6QUJBQkFCQUJBQkFCQUJBQkFCQUJBQkFCQUJBQkFCQUJBQkFCQUJBQg==';

app.use(bodyParser.json());

app.get('/ping', (request, response) => response.status(200).send('pong'));

app.get('/', (request, response) => response.sendFile(path.join(__dirname, 'build', 'index.html')));

app.get('/api/orders', (request, response) => axios.get('orders')
    .then(result => response.status(200).send({
        ...result.data,
        nextLink: result.data._links.next.href.replace('https://test.moip.com.br/v2/', ''),
    }))
    .catch(error => response.send(error)));

app.get('/api/orders/:orderId', (request, response) => axios.get(`/orders/${request.params.orderId}`)
    .then(result => response.status(200).send(result.data))
    .catch(error => response.send(error)));

app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, 'build')));
app.get('/ping', (req, res) => res.send('pong'));

app.get('/*', (req, res) => void res.sendFile(path.join(__dirname, 'build', 'index.html')));

app.listen(process.env.PORT || 8080, () => console.log(`Listening at port ${process.env.PORT || 8080}`));
